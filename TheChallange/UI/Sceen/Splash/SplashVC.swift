//
//  SplashVC.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Network
import UIKit

class SplashVC: BaseVC {

    @IBOutlet private weak var titleLable: UILabel!
    let monitor = NWPathMonitor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showLoader()
        checkInternetAvailability()
    }
    private func checkInternetAvailability() {
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                DispatchQueue.main.async {
                    self.titleLable.text = "Connected!"
                    self.titleLable.textColor = .green
                    self.prepareUI()
                }
            } else {
                DispatchQueue.main.async {
                    self.hideLoader()
                    self.titleLable.text = "No Connection!"
                    self.titleLable.textColor = .red
                }
            }
        }
        
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }
    
    func prepareUI() {
        
        self.hideLoader()
        self.titleLable.textColor = .orange
        self.titleLable.text = "Challenge Accepted"
        DispatchQueue.main.asyncAfter(deadline: .now() + 3,execute: self.startApplication())
        
    }
    
    func startApplication() -> (() -> Void) {
        return {
            self.show(TradeBuilder.make(), sender: nil)
        }
    }
    
}
