//
//  SplashViewModel.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

class SplashViewModel {
    private var remoteText:String = ""
    
    func fetchConfiguration(completion: @escaping () -> Void) {
        self.remoteText = "The Trader"
        DispatchQueue.main.async {
            completion()
        }
        
    }

    var text: String {
        remoteText
    }
    var connectionText: String {
        "Connected!"
    }
    var noConnectionText: String {
        "No Connection!"
    }
}
