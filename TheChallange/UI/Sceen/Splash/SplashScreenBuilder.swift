//
//  SplashScreenBuilder.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

final class SplashScreenBuilder: BaseBuilder {
    static func make() -> SplashVC {
        let controller: SplashVC = self.load(appStoryboard: .splashScreen, viewController: "SplashVC")
        return controller
    }
}
