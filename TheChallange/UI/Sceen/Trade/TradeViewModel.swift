//
//  TradeViewModel.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

final class TradeViewModel : TradeViewModelProtocol {
    var delegate: TradeViewModelDelegate?
    private let service: StockService
    
    init(service: StockService = StockService() ) {
        self.service = service
    }
    
    private var options: [PageDetail] = []
    private var symbolList: [Symbol] = []
    private var leftKey = ""
    private var rightKey = ""
    private var currentData: [TradeData] = []
    
    func updateData(leftKey: String?, rightKey: String?, completion: (() -> ())?) {
        self.service.getStockList { [unowned self] (result) in
            self.options.removeAll()
            self.symbolList.removeAll()
            switch (result) {
            case .success(let response):
                self.symbolList.append(contentsOf: response.mypageDefaults)
                self.options.append(contentsOf: response.mypage)
                self.leftKey = self.options[0].key
                self.rightKey = self.options[1].key
                if let leftKey = leftKey {
                    self.leftKey = leftKey
                }
                if let rightKey = rightKey {
                    self.rightKey = rightKey
                }
                let symbolKeys = self.symbolList.map{ symbol in symbol.tke}
                let optionKeys = [ConstantKeys.lastValue.rawValue, self.leftKey, self.rightKey]
                self.service.getStockListData(fieldIDs: optionKeys, stockIDs: symbolKeys) { (result) in
                    switch result {
                    case .success(let response):
                        self.handleNewData(newData: response)
                        completion?()
                    case .failure(let error):
                        print(error)
                        completion?()
                    }
                }
            case .failure(let error):
                print(error)
                completion?()
            }
        }
    }
    
    private func handleNewData(newData: StockDetail) {
        let carrierData = self.castDataCarrier(data: newData.stockDetails)
        self.currentData.removeAll()
        self.currentData.append(contentsOf: carrierData)
    }
    
    private func castDataCarrier(data: [StockDetailItem]) -> [TradeData] {
        var carrierList: [TradeData] = []
        for symbol in self.symbolList {
            let id = symbol.tke
            var detail: [String: String] = [:]
            for item in data {
                if item[ConstantKeys.id.rawValue] == id {
                    detail = item
                    break
                }
            }
            let time = detail[ConstantKeys.time.rawValue] ?? "-"
            let lastValue = detail[ConstantKeys.lastValue.rawValue] ?? "0"
            let leftValue = detail[self.leftKey] ?? "0"
            let rightValue = detail[self.rightKey] ?? "0"
            var isHighlighted = false
            var changeState = StockChangeState.noChange
            var leftValueState = StockChangeState.noChange
            var rightValueState = StockChangeState.noChange
            for prev in self.currentData {
                if prev.tradeID == id {
                    if prev.lastValue != time {
                        if prev.time != time {
                            isHighlighted = true
                        }
                        changeState = self.compareValues(previous: prev.lastValue, next: lastValue)
                        if self.leftKey == ConstantKeys.percentDiff.rawValue || self.leftKey == ConstantKeys.diff.rawValue {
                            leftValueState = compareValues(previous: prev.leftValue, next: leftValue)
                        }
                        if self.rightKey == ConstantKeys.percentDiff.rawValue || self.rightKey == ConstantKeys.diff.rawValue {
                            rightValueState = compareValues(previous: prev.rightValue, next: rightValue)
                        }
                    }
                    break
                }
            }
            let carrier = TradeData(tradeID: id, tradeName: symbol.cod, time: time, lastValue: lastValue, isHighlighted: isHighlighted, changeState: changeState, leftValue: leftValue, rightValue: rightValue, leftValueState: leftValueState, rightValueState: rightValueState)
            carrierList.append(carrier)
        }
        return carrierList
    }
    
    private func compareValues(previous: String, next: String) -> StockChangeState {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "tr_TR")
        guard let prevNumber = formatter.number(from: previous) else { return StockChangeState.noChange }
        guard let  nextNumber = formatter.number(from: next) else { return StockChangeState.noChange }
        let comparison = prevNumber.compare(nextNumber)
        switch comparison {
        case .orderedAscending:
            return StockChangeState.increasing
        case .orderedDescending:
        return StockChangeState.decreasing
        default:
            return StockChangeState.noChange
        }
    }
    
    func numberOfSymbols() -> Int {
        return self.currentData.count
    }
    
    func tradeDataAtIndex(atIndex: Int) -> TradeData {
        return self.currentData[atIndex]
    }
    
    func selectedKeys() -> (String, String) {
        return (self.leftKey, self.rightKey)
    }
    
    func keyList() -> [PageDetail] {
        return self.options
    }
    
    func nameOfKey(key: String) -> String {
        return self.options.first(where: {$0.key == key})?.name ?? "-"
    }
    
}
