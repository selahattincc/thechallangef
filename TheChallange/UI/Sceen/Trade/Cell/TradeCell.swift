//
//  TradeCell.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import UIKit

class TradeCell: UITableViewCell {

    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var symbolNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var leftValueLabel: UILabel!
    @IBOutlet weak var rightValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(symbol: TradeData) {
        self.symbolNameLabel.text = symbol.tradeName
        self.timeLabel.text = symbol.time
        self.leftValueLabel.text = symbol.leftValue
        self.rightValueLabel.text = symbol.rightValue
        self.backgroundColor = UIColor.clear
        if symbol.isHighlighted {
            self.backgroundColor = UIColor.darkGray
        }
        self.selectImage(state: symbol.changeState)
        self.leftValueLabel.textColor = self.selectColor(state: symbol.leftValueState)
        self.rightValueLabel.textColor = self.selectColor(state: symbol.rightValueState)
    }
    
    private func selectImage(state: StockChangeState) {
        switch state {
        case .increasing:
            self.arrowImageView.image = UIImage(systemName: "chevron.up") ?? UIImage()
            self.arrowImageView.tintColor = .white
            self.arrowImageView.backgroundColor = UIColor.green
        case .noChange:
            self.arrowImageView.image = UIImage()
            self.arrowImageView.backgroundColor = UIColor.clear
        case .decreasing:
            self.arrowImageView.image = UIImage(systemName: "chevron.down") ?? UIImage()
            self.arrowImageView.tintColor = .white
            self.arrowImageView.backgroundColor = UIColor.red
        }
    }
    
    private func selectColor(state: StockChangeState) -> UIColor {
        switch state {
        case .increasing:
            return UIColor.green
        case .noChange:
            return UIColor.white
        case .decreasing:
            return UIColor.red
        }
    }
}
