//
//  TradeVC.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import UIKit

class TradeVC: BaseVC {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerContainer: UIView!
    
    var viewModel: TradeViewModelProtocol?
    
    var timer: Timer?
    var leftSelection: String?
    var rightSelection: String?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.updateData()
        self.startTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimer()
    }
    
    func updateData() {
        if let viewModel = self.viewModel{
            viewModel.updateData(leftKey: self.leftSelection, rightKey: self.rightSelection) { [unowned self] in
                self.pickerView.reloadAllComponents()
                self.tableView.reloadData()
                if self.leftSelection == nil && viewModel.keyList().count > 0 {
                    self.leftSelection = viewModel.keyList()[0].key
                    self.pickerView.selectRow(0, inComponent: 0, animated: false)
                }
                if self.rightSelection == nil && viewModel.keyList().count > 1 {
                    self.rightSelection = viewModel.keyList()[1].key
                    self.pickerView.selectRow(1, inComponent: 1, animated: false)
                }
                self.leftButton.setTitle(viewModel.nameOfKey(key: self.leftSelection!), for: .normal)
                self.rightButton.setTitle(viewModel.nameOfKey(key: self.rightSelection!), for: .normal)
            }
        }
        
    }
    
    func startTimer() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [unowned self] (timer) in
            self.updateData()
        })
    }
    
    func stopTimer() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        self.pickerContainer.isHidden = false
        self.view.bringSubviewToFront(self.pickerContainer)
    }
    @IBAction func pickerCloserPressed(_ sender: Any) {
        self.pickerContainer.isHidden = true
        let leftIndex = self.pickerView.selectedRow(inComponent: 0)
        let rightIndex = self.pickerView.selectedRow(inComponent: 1)
        self.stopTimer()
        self.leftSelection = self.viewModel?.keyList()[leftIndex].key
        self.rightSelection = self.viewModel?.keyList()[rightIndex].key
        self.leftButton.setTitle(self.viewModel?.nameOfKey(key: self.leftSelection!), for: .normal)
        self.rightButton.setTitle(self.viewModel?.nameOfKey(key: self.rightSelection!), for: .normal)
        self.updateData()
        self.startTimer()
    }
    
}
extension TradeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 * HEIGHT_FACTOR_IPHONE
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.numberOfSymbols() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TradeCell", for: indexPath) as? TradeCell {
            let stock = self.viewModel?.tradeDataAtIndex(atIndex: indexPath.row)
            if let stock = stock {
                cell.configureCell(symbol: stock)
            }
            return cell
        }
        return UITableViewCell()
    }
}

extension TradeVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.viewModel?.keyList().count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.viewModel?.nameOfKey(key: self.viewModel?.keyList()[row].key ?? "")
    }
}
