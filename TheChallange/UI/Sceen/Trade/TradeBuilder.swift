//
//  TradeBuilder.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation


final class TradeBuilder: BaseBuilder {
    static func make() -> TradeVC {
        let controller: TradeVC = self.load(appStoryboard: .tradeScreen, viewController: "TradeSceen")
        controller.viewModel = TradeViewModel()
        
        return controller
    }
}
