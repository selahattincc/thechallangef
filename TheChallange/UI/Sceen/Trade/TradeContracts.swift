//
//  TradeContracts.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

enum ConstantKeys: String {
    case id = "tke"
    case time = "clo"
    case lastValue = "las"
    case percentDiff = "pdd"
    case diff = "ddi"
}

protocol TradeViewModelDelegate: AnyObject {
    
}

protocol TradeViewModelProtocol: AnyObject {
    var delegate: TradeViewModelDelegate? { get set }
    
    func updateData(leftKey: String?, rightKey: String?, completion: (() -> ())?)
    func numberOfSymbols() -> Int
    func tradeDataAtIndex(atIndex: Int) -> TradeData
    func selectedKeys() -> (String, String)
    func keyList() -> [PageDetail]
    func nameOfKey(key: String) -> String
    
}
