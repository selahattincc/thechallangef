//
//  BaseNavigationController.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import UIKit

class BaseNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    func setup() {
        self.setNavigationBarHidden(true, animated: false)
    }
}
