//
//  BaseVC.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation
import UIKit

class BaseVC: UIViewController {
    let loadingView: LoadingIndicator = {
        let progress = LoadingIndicator(colors: [.red, .systemGreen, .systemBlue], lineWidth: 5)
        progress.translatesAutoresizingMaskIntoConstraints = false
        return progress
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }
    
    private func setupUI() {
        overrideUserInterfaceStyle = .light
        self.view.addSubview(loadingView)
        
        NSLayoutConstraint.activate([
            loadingView.centerXAnchor
                .constraint(equalTo: self.view.centerXAnchor),
            loadingView.centerYAnchor
                .constraint(equalTo: self.view.centerYAnchor),
            loadingView.widthAnchor
                .constraint(equalToConstant: 50),
            loadingView.heightAnchor
                .constraint(equalToConstant: 50)
        ])
        view.bringSubviewToFront(loadingView)
    }
    
    func showLoader(){
        self.loadingView.isAnimating = true
    }
    
    func hideLoader(){
        self.loadingView.isAnimating = false
    }
    
}
