//
//  StoryboardContants.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

enum AppSceneName: String {
    case splashScreen = "Splash"
    case tradeScreen = "Trade"
}
