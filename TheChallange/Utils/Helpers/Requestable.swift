//
//  Requestable.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

protocol Requestable{
    var urlRequest: URLRequest { get set }
}
