//
//  ResultCompletion.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

typealias ResultCompletion<T: Codable> = (Result<T,Error>) -> Void
