//
//  TradeListDetailRequest.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

class TradeListDataRequest: Requestable {
    var urlRequest: URLRequest
    init(fieldIDs: [String], stockIDs: [String]) {
        let urlString = Configuration.TLIST_DETAIL_API + "?fields=" + fieldIDs.joined(separator: ",") + "&stcs=" + stockIDs.joined(separator: "~")
        let url = URL(string: urlString)!
        urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
    }
}
