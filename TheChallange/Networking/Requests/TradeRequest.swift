//
//  TradeRequest.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

/// request sample url:  "https://sui7963dq6.execute-api.eu-central-1.amazonaws.com/default/ForeksMobileInterviewSettings"
class TradeListRequest: Requestable {
    var urlRequest: URLRequest
    init() {
        let urlString = Configuration.TLIST_API
        let url = URL(string: urlString)!
        urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
    }
}


