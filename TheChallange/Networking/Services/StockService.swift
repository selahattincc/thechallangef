//
//  StockService.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation


final class StockService: Service {
    
    func getStockList(completion: @escaping ResultCompletion<SymbolListResponse>) {
        let request = TradeListRequest()
        makeRequest(request: request, completion: completion)
    }
    func getStockListData(fieldIDs: [String],stockIDs: [String],completion: @escaping ResultCompletion<StockDetail>) {
        let request = TradeListDataRequest(fieldIDs: fieldIDs,stockIDs: stockIDs)
        makeRequest(request: request, completion: completion)
    }
}
