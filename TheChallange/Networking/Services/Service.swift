//
//  Service.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

class Service {

    final func makeRequest<Response: Codable>(request: Requestable, completion: @escaping ResultCompletion<Response>) {
        if Reachability.isConnectedToNetwork(){
            URLSession.shared.dataTask(with: request.urlRequest) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            }else if let data = data {
                let decoder = JSONDecoder()
                do {
                    let response = try decoder.decode(Response.self, from: data)
                    DispatchQueue.main.async {
                        completion(.success(response))
                    }
                }catch {
                    DispatchQueue.main.async {
                        completion(.failure(error))
                    }
                }
                
            }else {
                completion(.failure(NSError(domain: "com.foreks.network", code: 999, userInfo: nil)))
            }
        }.resume()
        
        }else{
            completion(.failure(NSError(domain: "com.foreks.network", code: 999, userInfo: nil)))
        }
        
    }
}
