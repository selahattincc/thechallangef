//
//  StockListDetail.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation


struct Symbol: Codable {
    let cod: String
    let def: String
    let gro: String
    let tke: String
}

struct PageDetail: Codable {
    let key: String
    let name: String
}

struct SymbolListResponse: Codable {
    let mypageDefaults: [Symbol]
    let mypage: [PageDetail]
}
