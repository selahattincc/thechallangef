//
//  StockList.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation

typealias StockDetailItem = [String: String]

struct StockDetail: Codable {
    
    enum StockKeys: String, CodingKey {
        case list = "l"
    }
    
    let stockDetails: [StockDetailItem]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StockKeys.self)
        self.stockDetails = try container.decode(Array.self, forKey: .list)
    }
}
