//
//  TradeData.swift
//  TheChallange
//
//  Created by Selahattin on 8.08.2021.
//

import Foundation


enum StockChangeState {
    case increasing
    case noChange
    case decreasing
}

struct TradeData {
    let tradeID: String
    let tradeName: String
    let time: String
    let lastValue: String
    let isHighlighted: Bool
    let changeState: StockChangeState
    let leftValue: String
    let rightValue: String
    let leftValueState: StockChangeState
    let rightValueState: StockChangeState
}
